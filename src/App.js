// src/App.js
import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'; // Make sure you're importing Switch directly
import Navbar from './Components/Navbar/Navbar';
import Footer from './Components/Footer/Footer';
import Contact from './Components/Contact';
import Accueil from './Components/Accueil/Accueil';
import Ateliers from './Components/Ateliers';
import Boutique from './Components/Boutique';
import About from './Components/About';
import Error from './Components/Error';



const App = () => {
  return (
      <div>
        <Navbar />
      <Routes>
        <Route path="/" element={ <Accueil/> } />
        <Route path="contact" element={ <Contact/> } />
        <Route path="boutique" element={ <Boutique/> } />
        <Route path="ateliers" element={ <Ateliers/> } />
        <Route path="about" element={ <About/> } />
        <Route path="*" element={<Error />} />

      </Routes>
        <Footer />
      </div>
  );
};

export default App;
