import React from 'react';
import { Link } from 'react-router-dom';
import "./navbar.css"

const Navbar = () => {
  return (

    <nav> 

      <h1>LE PETIT ATELIER DE MATHILDE</h1>

       <ul>
        <li>
          <Link to="/">Accueil</Link>
        </li>
        <li>
          <Link to="/about">Qui suis-je?</Link>
        </li>
         <li>
          <Link to="/boutique">Boutique</Link>
        </li>
         <li>
          <Link to="/ateliers">Mes ateliers</Link>
        </li>
         <li>
          <Link to="/contact">Me contacter</Link>
          
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;
