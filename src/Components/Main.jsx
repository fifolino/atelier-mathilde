import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Accueil from "./Components/Accueil";
import Contact from "./Components/Contact";

const Main = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Accueil />}>
          <Route path='/contact' element={<Contact />} />
        </Route>
      </Routes>
    </BrowserRouter>
  )
}

export default Main;