import './accueil.css';
import mathilde from '../../img/mathilde.jpg';
import InstagramFeed from './InstaFeed';

const Accueil = () => {
  return (
        <>
        <section id="presentation">
              <img src={mathilde} alt="image de mathilde mary" id='mathilde-accueil'/>
          <article id='text-pres'>
            <h2>Texte de présentation</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac leo non libero vestibulum accumsan. Proin ut venenatis metus. Nam id sagittis libero. Nunc at diam id elit ullamcorper scelerisque. Duis gravida, dolor et tristique eleifend, mi massa sollicitudin purus, vel feugiat tellus nisi ac justo. Fusce non sagittis massa. Duis hendrerit imperdiet felis, ut malesuada quam facilisis vel. Pellentesque nec tellus id odio feugiat scelerisque. Vivamus vel dui eget nisi aliquet malesuada. Ut vel tincidunt libero. Fusce ultricies arcu vel erat dignissim, eu malesuada elit volutpat.
            </p>
            <p>
              Praesent ullamcorper, mauris vel eleifend lacinia, lacus justo ultrices quam, eu lacinia odio quam in nisi. Curabitur aliquet massa vel dolor malesuada dignissim. Aliquam erat volutpat. Aenean vel rhoncus orci, ac commodo elit. Maecenas cursus odio vel lacus tempor, vel efficitur libero facilisis. In hac habitasse platea dictumst. Integer sit amet consequat massa, vitae eleifend odio. Nam vel neque eu lacus pellentesque suscipit. Sed gravida, quam eu mattis blandit, sem arcu aliquet ex, ut hendrerit risus ex ut sapien. Nullam et orci et nisl viverra commodo.
            </p>
          </article>
          </section>
          <section id="insta-pres">
            <hr />
            <h2>Suivez mes aventures sur instagram !</h2>
            <InstagramFeed/>
          </section>
        </>
  )
};

export default Accueil;
