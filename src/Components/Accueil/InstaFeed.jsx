import React, { useEffect, useState } from 'react';
import axios from 'axios';

const InstagramFeed = () => {
  const [photos, setPhotos] = useState([]);

  useEffect(() => {
    // Appel à l'API d'Instagram
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `https://graph.instagram.com/v18.0/me/media?fields=id,caption,media_type,media_url,thumbnail_url,permalink,timestamp&access_token=f54bf325f671560e0ee42486a185c3e1`
        );

        // Mettre à jour l'état avec les données des 3 dernières photos
        setPhotos(response.data.data.slice(0, 3));
      } catch (error) {
  console.error('Erreur lors de la récupération des photos Instagram', error);
  console.error('Details de l\'erreur Axios:', error.response.data);
}
    };

    fetchData();
  }, []);

  return (
    <div>
      <h1>Instagram Feed</h1>
      {photos.map((photo) => (
        <div key={photo.id}>
          <img src={photo.media_url} alt={photo.caption} />
          <p>{photo.caption}</p>
        </div>
      ))}
    </div>
  );
};

export default InstagramFeed;
